# Ubuntu Touch for Samsung Galaxy S10, S10e, S10+, Note10, Note10+ Exynos (beyondxlte, d1, d1x, d2s, d2x)

# Status
Check https://devices.ubuntu-touch.io/device/beyondxlte/ for up-to-date status.


Please make a issue for things that do not work that *arent* on the page.

# Building

To build by hand, run these commands;

```
./build.sh -b bd  # bd is the name of the build directory
./build/prepare-fake-ota.sh out/device_samsung-beyond1lte.tar.xz ota
./build/system-image-from-ota.sh ota/ubuntu_command out
```

# Installation

To install, follow these steps;

- Get the vendor image for your device from [here](https://github.com/temp-utvendor/prebuilt-vendor-images)
- Wipe data and system
- Flash ubuntu.img onto system
- Flash boot.img onto boot
- Flash vendor image onto vendor

