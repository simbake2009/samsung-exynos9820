#!/bin/bash
set -xe

[ -d build ] || git clone https://gitlab.com/simbake2009/halium-generic-adaptation-build-tools.git build

cd build

[ -d vendor ] || git clone https://github.com/TheMuppets/proprietary_vendor_samsung_exynos9820-common.git vendor

cd -

./build/build.sh "${args[@]}" -b build
